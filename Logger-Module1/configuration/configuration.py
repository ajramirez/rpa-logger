import json
import os
import logging
import time
import os.path as path
from pathlib import Path

json_file_concat = []

'''
Edit the server configuration in the configuration_properties file
'''
def edit_server_configuration(key,value):
    pathA = os.getcwd()
    f = open(pathA+"/configuration/configuration_properties.json", "r")
    config = json.loads(f.read())
    f.close()
    fi = open(pathA+"/configuration/configuration_properties.json", "w")
    config[key] = value
    config = json.dumps(config)
    fi.write(config)
    fi.close()

'''
Get the capture status
'''
def status_connection():
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        status = config["captures"]["status"]
        f.close()
    except:
        status = None
    return status

'''
Get the project name
'''
def project_name():
    projectName = "tmp"
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        projectName = config["projectName"]
        f.close()
        return projectName
    except:
        return projectName

'''
Get the capture name
'''
def capture_name():
    captureName = "tmp"
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        captureName = config["captures"]["timestamp"]
        f.close()
        return str(captureName)
    except:
        return str(captureName)

'''
Get the server URL
'''
def server_url():
    pathA = os.getcwd()
    f = open(pathA+"/configuration/configuration_properties.json", "r")
    config = json.loads(f.read())
    projectName = config["server"]
    f.close()
    return projectName

'''
Generate the configuration file if the file doesn't exist
'''
def create_configuration():
    pathA = os.getcwd()
    if not path.exists(pathA + "/configuration"):
        Path(pathA + "/configuration").mkdir(parents=True, exist_ok=True)
    if not path.exists(pathA + "/configuration/configuration_properties.json"):
        f = open(pathA + "/configuration/configuration_properties.json", "w")
        config = {"projectName": "tmp", "description": "temporal project in the local logger", "capturePolicy": {"name": "TMP", "description": "Capture policy in the local logger if you don't have a policy configured.", "default": False, "modules": [], "actions": [{"keyNumber": 1, "triggerKey": None, "keyboard": {"keywordNumber": 10}, "mouse": None, "image": None}, {"keyNumber": 1, "triggerKey": None, "keyboard": None, "mouse": 0, "image": None}, {"keyNumber": 1, "triggerKey": None, "keyboard": None, "mouse": None, "image": {"changeApp": True, "periodTime": 10, "inactiveTime": 10}}]}, "sendPolicy": {"name": "TMP", "eventNumber": 10, "connectionTime": 10, "imageLimit": False, "description": "Send policy in the local logger if you don't have a policy configured.", "default": False}, "captures": {"status": True, "timestamp": "tmp"}, "server": ""}
        json.dumps(config)
        file = json.dumps(config)
        f.write(file)
        f.close()

'''
Generate the capture directory if the directory doesn't exist
'''
def create_capture_project():
    pathA = os.getcwd()
    captureName = capture_name()
    projectName = project_name()
    if not path.exists(pathA + "/captures/" + projectName + "/" + captureName):
        Path(pathA + "/captures/" + projectName + "/" + captureName).mkdir(parents=True, exist_ok=True)

'''
Generic method for calling up functions with exception control
'''
def exception_controller(func, *args):
    try:
        value = func(*args)
    except IOError as error:
        logging.exception("Error: -> "+error)
    return value


'''
Get the senPolicy value
'''
def send_policy(valor):
    send = None
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        send = config["sendPolicy"][valor]
        f.close()
        return send
    except:
        return send

'''
Get the capturePolicy value
'''
def capture_policy(valor):
    capture = None
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        capture = config["capturePolicy"][valor]
        f.close()
        return capture
    except:
        return capture
'''
Get the molude value from capturePolicy
'''
def modules_policy(valor):
    module = False
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        for i in config["capturePolicy"]["modules"]:
            if i["name"] is not None:
                if i["name"] == valor:
                    module = True
        f.close()
        return module
    except:
        return module

'''
Get the actions value from capturePolicy
'''
def actions_policy(valor):
    action = None
    try:
        pathA = os.getcwd()
        f = open(pathA+"/configuration/configuration_properties.json", "r")
        config = json.loads(f.read())
        for i in config["capturePolicy"]["actions"]:
            if i[valor] is not None:
                action = i
                break
        f.close()
        return action
    except:
        return action

'''
Events list
'''
def json_file_concat_list():
    global json_file_concat
    return json_file_concat

'''
Add event to the event list
'''
def json_file_concat_add(value):
    global json_file_concat
    json_file_concat.append(value)
    return json_file_concat

'''
Reset event list by a value
'''
def json_file_concat_list_reset(value):
    global json_file_concat
    if value is None:
        json_file_concat = []
    else:
        try:
            json_file_concat = json_file_concat[value:]
        except:
            return json_file_concat
    return json_file_concat
