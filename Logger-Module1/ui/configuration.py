from tkinter import *
from ui.menu import *
from configuration.configuration import edit_server_configuration
from logger.keylog import *
from logger.mouse import *
from tkinter import messagebox
from main import *
from logger.screenshot import stop_image,start_image_timer_period,start_image_timer_inactive,json_file_imagen_list
from conection.connector import *
from main import generate_json_screenshot, generate_json

en = ""
lb = ""
b1 = ""
b2 = ""
status = ""
keyboard = ""
mouse = ""
thread = ""

def configure_server(top,name,menuTop):
    global lb
    global en
    global b1
    global b2
    '''
    Change the configuration_properties file with the server url
    '''
    def get_text_conf(self):
        inputBox = en.get()
        save = exception_controller(edit_server_configuration, "server", inputBox)
        en.destroy()
        lb.destroy()
        return save
    '''
    Change the configuration_properties file with the project name
    '''
    def get_text_project(self):
        inputBox = en.get()
        save = exception_controller(edit_server_configuration,"projectName", inputBox)
        en.destroy()
        lb.destroy()
        return save

    if name=="Project":
        '''
        Project UI
        '''
        destroy_entry(en)
        destroy_entry(lb)
        destroy_entry(b1)
        destroy_entry(b2)
        projectName = project_name()
        lb = Label(top, text="Actual project: " + projectName + " - Change Local project: ")
        lb.pack(side=LEFT)
        en = Entry(top)
        en.bind("<Return>", get_text_project)
        en.pack(side=LEFT)
    elif name=="Configuration":
        '''
        Configuration UI
        '''
        destroy_entry(en)
        destroy_entry(lb)
        destroy_entry(b1)
        destroy_entry(b2)
        configuration = server_url()
        actualServer = ""
        if configuration:
            actualServer = "Actual server: " + configuration + "- To use the server configured, restart the program."
        lb = Label(top, text=actualServer + " - Change Server URL: ")
        lb.pack(side=LEFT)
        en = Entry(top)
        en.bind("<Return>", get_text_conf)
        en.pack(side=LEFT)
    elif name =="Logger":
        '''
        Logger UI
        '''
        destroy_entry(en)
        destroy_entry(lb)
        destroy_entry(b1)
        destroy_entry(b2)
        logger(top,menuTop)

'''
Destroys interface elements
'''
def destroy_entry(ent):
    if ent:
        ent.destroy()

'''
User logger controller
'''
def logger(top,menuTop):
    global lb
    global en
    global b1
    global b2

#start do status a true,minimize the window and disable the menu #self.menubar.entryconfig("Test2", state="normal")
#end do status a false and enable menu
    def start_logger(top,menuTop):
        global status
        global keyboard
        global mouse
        global b1
        global b2
        top.wm_state('iconic')
        menuTop.entryconfig("Configuration", state="disable")
        menuTop.entryconfig("Project", state="disable")
        menuTop.entryconfig("Logger", state="disable")
        b1.config(state="disable")
        b2.config(state="normal")
        status = True
        create_capture_project()
        keyboard = exception_controller(control_keyboard_detection)
        mouse = exception_controller(control_mouse_detection)
        exception_controller(start_image_timer_period)
        exception_controller(start_image_timer_inactive)
        exception_controller(start_write_file_offline)


    def stop_logger(top,menuTop):
        global status
        global keyboard
        global mouse
        global b1
        global b2
        status = False
        menuTop.entryconfig("Configuration", state="normal")
        menuTop.entryconfig("Project", state="normal")
        menuTop.entryconfig("Logger", state="normal")
        b1.config(state="normal")
        b2.config(state="disable")
        exception_controller(stop_image)
        exception_controller(stop_mouse_detection, mouse)
        exception_controller(stop_keyboard_detection, keyboard)
        exception_controller(stop_write_file_offline)

    def on_closing():
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            global status
            if status:
                stop_logger(top, menuTop)
            top.destroy()
            sys.exit()

    top.protocol("WM_DELETE_WINDOW", on_closing)
    lb = Label(top, text="Start Logger. If stop the logger, please wait until the process is over. Can take a few minutes.")
    lb.pack(side=LEFT)
    b1 = Button(top, text="Start", command=lambda: start_logger(top,menuTop))
    b2 = Button(top, text="Stop", command=lambda: stop_logger(top,menuTop))
    b1.pack(side=LEFT)
    b2.pack(side=LEFT)

def start_write_file_offline():
    global thread
    thread = threading.Thread(target =test_event_number_offline)
    thread.name = "event_number_offline"
    thread.start()
    return thread

def stop_write_file_offline():
    global thread
    if thread != "":
        if thread.is_alive():
            thread.join()

'''
optimizing offline file writing
'''
def test_event_number_offline():
    eventNumber = send_policy("eventNumber")
    if eventNumber is None:
        eventNumber = 1
    json_concat = json_file_concat_list()+json_file_imagen_list()
    if len(json_concat) >= eventNumber:
        json_events = generate_json(json_file_concat_list()[0:eventNumber])
        json_file_concat_list_reset(len(json_events))
        json_screenshot = generate_json_screenshot(json_file_imagen_list()[0:eventNumber])
        json_file_imagen_list_reset(len(json_screenshot))