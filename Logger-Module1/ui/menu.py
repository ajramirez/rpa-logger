from tkinter import *
from ui.configuration import *

'''
Message when you close the window
'''
def on_closing_menu():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        sys.exit()

'''
Top menu in the UI with Configuration, Project and Logger buttons
'''
def main_menu():
    top = Tk()
    top.title('Logger')
    menuTop = Menu(top)
    menuTop.add_command(label= "Configuration", command = lambda: configure_server(top,"Configuration",menuTop))
    menuTop.add_command(label= "Project", command = lambda: configure_server(top,"Project",menuTop))
    menuTop.add_command(label= "Logger", command = lambda: configure_server(top,"Logger",menuTop))
    top.protocol("WM_DELETE_WINDOW", on_closing_menu)
    top.config(menu=menuTop)
    top.mainloop()

