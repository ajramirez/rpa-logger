import platform
from psutil import virtual_memory
import re, uuid
import socket
import getpass
import os
import shutil
import json
import os.path as path
from pathlib import Path

'''
The specifications of a PC
'''
def specifications_PC():
    user = getpass.getuser()
    """
    (system, node, release, version, machine, processor)
    """
    spec = platform.uname()
    ram = round(virtual_memory().total/(1024*1024))
    mac = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
    cores = os.cpu_count()
    total, used, free = shutil.disk_usage("/")
    capture = {
        "MAC" : mac,
        "namePC": spec[1],
        "userPC": user,
        "system": spec[0],
        "release": spec[2],
        "version": spec[3],
        "machine": spec[4],
        "processor": spec[5],
        "core": cores,
        "RAM": ram,
        "primaryDisk": round((total/1048576),2)
    }
    file = write_to_json(capture)
    return file

"""
Write the specifications in a json
"""
def write_to_json(value):
    pathA = os.getcwd()
    if not path.exists(pathA + "/captures"):
        Path(pathA + "/captures").mkdir(parents=True, exist_ok=True)
    f = open(pathA + "/captures/PC.json", "w")
    json_file = json.dumps(value)
    f.write(json_file)
    f.close()
    return json_file

"""
Read the specifications from a json
"""
def get_json():
    path = os.getcwd()
    Path(path + "/captures").mkdir(parents=True, exist_ok=True)
    f = open(path + "/captures/PC.json", "r")
    json_file = f.read()
    f.close
    return json_file
