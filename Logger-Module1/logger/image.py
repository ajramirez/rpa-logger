import pyautogui
from pathlib import Path
from configuration.configuration import *
from logger.foregrounapp import *
import tkinter as tk
import os

json_file_imagen = []


"""
If a key or mouse event had captured, the imagen is created
"""
def capture_image(moment):
    projectName = project_name()
    if projectName =="":
        projectName = "tmp"
    #if the dir isn't exist, the function create a new dir
    path = os.getcwd()
    Path(path+"\\captures\\"+projectName).mkdir(parents=True, exist_ok=True)
    img = pyautogui.screenshot()
    name = str(moment)
    img.save(path+"\\captures\\"+projectName+"\\"+name+".png")
    resolution = screen_resolution()
    nameAPP = active_window()
    values = [nameAPP,resolution,moment]
    write_json_imagen(values)

def screen_resolution():
    inter = tk.Tk()
    x= inter.winfo_screenheight()
    y = inter.winfo_screenwidth()
    resolution = (y,x)
    return resolution

def write_json_imagen(values):
    global json_file_imagen
    json_file = {
        "nameAPP" : values[0],
        "type": "SCREENSHOT",
        "coordinate": None,
        "duration": None,
        "content": "",
        "clipboard": "",
        "size": values[1],
        "timestamp":  values[2]
    }
    json_file_imagen.append(json_file)
    return json_file

def control_image_detection():
    global json_file_imagen
    json_file_imagen = []
    return json_file_imagen

def stop_image(th):
    global stop_thread
    stop_thread = True
    if th.is_alive():
        th.join()

def json_file_imagen_list():
    global json_file_imagen
    return json_file_imagen

