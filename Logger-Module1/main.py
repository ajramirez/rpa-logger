from logger.specificationsPC import specifications_PC
from configuration.configuration import create_configuration, exception_controller, create_capture_project, status_connection, json_file_concat_list
from conection.connector import *
from ui.menu import *
from logger.keylog import *
from logger.mouse import *
from logger.clipboard import *
from logger.screenshot import *
import os.path as path
from pathlib import Path

'''
Main method that calls other methods to automate the use of the software
'''
def status_validation():
    global statusConnection
    #enviar al servidor la información del PC
    create_configuration()
    pc = exception_controller(specifications_PC)
    if pc == None:
        return False, False
    flag,status = exception_controller(connection)
    statusConnection = status
    if not flag:
        return main_menu()
        # si no está correcto, permite el control por parte del usuario
        # preguntar por configuración o proyecto y setear en configuration properties
        #
        # si se setea configuration, volver al primer paso
        #
        # si se setea el proyecto, continuar con las ordenes del usuario
    elif statusConnection:
        create_capture_project()
        # volver a preguntar al servidor cada minuto
        #comenzar capturas
        keyboard = exception_controller(control_keyboard_detection)
        mouse = exception_controller(control_mouse_detection)
        exception_controller(start_image_timer_period)
        exception_controller(start_image_timer_inactive)
        exception_controller(control_connection)
        while not get_thread_status():
            test_event_number()
            statusConnection = status_connection()
            if not statusConnection:
                #terminar capturas
                exception_controller(stop_image)
                exception_controller(stop_connection)
                exception_controller(stop_mouse_detection, mouse)
                exception_controller(stop_keyboard_detection, keyboard)
                #llamar función
        return status_validation()
    else:
        #volver a preguntar al servidor cada minuto
        time.sleep(send_policy("connectionTime"))
        return status_validation()

'''
Generates a json file with the content of the screenshots
'''
def generate_json_screenshot(json_concat):
    if json_concat:
        json_file = json.dumps(json_concat)
        projectName = project_name()
        captureName = capture_name()
        pathA = os.getcwd()
        path_file = pathA + "/captures/" + projectName + "/" + captureName + "/Screenshots.json"
        f = open(path_file, "a+")
        if os.path.exists(path_file):
            f.write("\n")
        f.write(json_file)
        f.close()
        return json_file

'''
Generates a json file with the content of the events
'''
def generate_json(json_concat):
    if json_concat:
        actions = sorted(json_concat, key = lambda i: i['timestamp'])
        json_file = json.dumps(actions)
        projectName = project_name()
        captureName = capture_name()
        pathA = os.getcwd()
        path_file = pathA+"/captures/" + projectName + "/"+captureName+"/Events.json"
        f = open(path_file, "a+")
        if os.path.exists(path_file):
            f.write("\n")
        f.write(json_file)
        f.close()
        return json_file

'''
Check if the capture events number can be sent according to the values defined in the sendPolicy
'''
def test_event_number():
    global statusConnection
    eventNumber = send_policy("eventNumber")
    if eventNumber is None:
        eventNumber = 0
    json_concat = json_file_concat_list()+json_file_imagen_list()
    if statusConnection:
        if len(json_concat) >= eventNumber:
            send_capture(eventNumber)
    else:
        while len(json_file_concat_list())>=eventNumber:
            send_capture(eventNumber)
        send_capture(json_file_concat_list())

'''
Call the post function
'''
def send_capture(eventNumber):
    jsonEvents = generate_json(json_file_concat_list()[0:eventNumber])
    post_events(jsonEvents)
    jsonScreenshot = generate_json_screenshot(json_file_imagen_list()[0:eventNumber])
    post_screenshot(jsonScreenshot)

if __name__ == '__main__':
    validator = True
    while True:
        status = status_validation()

