from django.apps import AppConfig

class Note(AppConfig):
    name = 'notes'