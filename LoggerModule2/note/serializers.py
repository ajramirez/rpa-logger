from rest_framework import serializers
from Server.models import Note
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *

class NoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Note
        fields = ('id','note','configuration')

    def validate(self, attrs):

            data = attrs

            if data.get("note"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('note'),1000000),401, 'ERROR_NOTE_TEXT')
            return data

class ShortNoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Note
        fields = ('id','note')