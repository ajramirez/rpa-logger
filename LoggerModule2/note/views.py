from rest_framework import status
from Server.models import Configuration, Process
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework import permissions


class NoteManager(generics.DestroyAPIView):
    queryset = Configuration.objects.all()
    serializer_class = NoteSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Note.objects.get(pk=pk)
        except Note.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Delete a note by id.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            note = self.get_object(pk)
            note.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListNote(generics.CreateAPIView):
    serializer_class = NoteSerializer

    def get_queryset(self):
        return Note.objects.all()

    @swagger_auto_schema(operation_description="Create a note in a configuration.")
    def post(self, request, *args, **kwargs):

        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = NoteSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


