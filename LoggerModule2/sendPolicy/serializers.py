from rest_framework import serializers
from Server.models import Project, Capture, SendPolicy, CapturePolicy
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *


class SendPolicySerializer(serializers.ModelSerializer):

    class Meta:
        model = SendPolicy
        fields = ('id','name','eventNumber','connectionTime','imageLimit','description','default')

    def validate(self, attrs):

            data = attrs
            if data.get("name"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('projectName'), 255),401, ('ERROR_NAME_LENGHT'))
            if data.get("eventNumber"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('eventNumber'), 10000000),401, ('ERROR_EVENTNUMBER_LENGHT'))
            if data.get("connectionTime"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('connectionTime'), 10000000),401, ('ERROR_CONNECTIONTIME_LENGHT'))
            if data.get("imageLimit"):
                Assertions.assert_true_raise(((data.get('imageLimit') is True or False)), 401,('ERROR_IMAGELIMIT_TYPE'))
            if data.get("description"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('description'), 10000000), 401,('ERROR_DESCRIPTION_LENGHT'))
            return data


'''
Check if there is a predefined policy or if the policy being modified is the prefined policy 
'''
def default_validator(pk,data):
    if data.get("default"):
        default = SendPolicy.objects.filter(default=True).first()
        if pk is not None and default is not None:
            Assertions.assert_true_raise(int(pk) == default.id, 400, ('ERROR_DEFAULT_EXIST'))
        else:
            Assertions.assert_true_raise(False, 400, ('ERROR_DEFAULT_EXIST'))
