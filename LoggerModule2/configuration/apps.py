from django.apps import AppConfig

class Configuration(AppConfig):
    name = 'configurations'