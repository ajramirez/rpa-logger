from rest_framework import status
from Server.models import Configuration, Process
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user,get_server_token_access
from utils.Assertions import Assertions
from .serializers import *
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework import permissions


class ConfigurationManager(generics.RetrieveAPIView):
    queryset = Configuration.objects.all()
    serializer_class = ConfigurationSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Configuration.objects.get(pk=pk)
        except Configuration.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get a configuration by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        serverRPA = get_server_token_access(request, "RPA")
        if loggedUser is not None or serverRPA is not None:
            if pk is None:
                pk = self.kwargs['pk']
            configuration = self.get_object(pk)
            serializer = ConfigurationSerializer(configuration)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Delete a configuration by id. This Delete also removes groups, equal, delete and process associates.")
    @transaction.atomic
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            configuration = self.get_object(pk)
            configuration.delete()
            process = Process.objects.filter(groups=configuration.group_set.first())
            process.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListConfiguration(generics.ListCreateAPIView):
    serializer_class = ShortConfigurationSerializer

    def get_queryset(self):
        return Configuration.objects.all()
    project = openapi.Parameter('project', openapi.IN_QUERY, description="List by project id.", type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(operation_description="List configurations.",manual_parameters=[project])
    def get(self, request):
        loggedUser = get_logged_user(request)
        serverRPA = get_server_token_access(request, "RPA")
        if loggedUser is not None or serverRPA is not None:
            try:
                projectId = request.GET['project']
                queryset = Configuration.objects.filter(project=projectId)
            except:
                queryset = Configuration.objects.all()
            serializer = ConfigurationSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Create a configuration. The configuration generate groups by fingerprint calling a external service, if produce a Expectation Failed error, try to do a get configuration later to check if the configuration has benn created. The umbral is a percentage similarity threshold.")
    def post(self, request, *args, **kwargs):

        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                serializer = ShortConfigurationSerializer(data=request.data,partial=False)
                if serializer.is_valid():
                    configuration = serializer.save()
                    if call_processing(configuration):
                        return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(serializer.data, status=status.HTTP_417_EXPECTATION_FAILED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
