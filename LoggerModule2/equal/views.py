from rest_framework import status
from Server.models import Equal
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class EqualManager(generics.RetrieveAPIView):
    queryset = Equal.objects.all()
    serializer_class = EqualSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Equal.objects.get(pk=pk)
        except Equal.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get an equal/group by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            equal = self.get_object(pk)
            serializer = EqualSerializer(equal)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Delete an equal/group by id.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            equal = self.get_object(pk)
            equal.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListEqual(generics.ListCreateAPIView):
    serializer_class = EqualSerializer

    def get_queryset(self):
        return Equal.objects.all()
    configuration = openapi.Parameter('configuration', openapi.IN_QUERY, description="List by configuration id.", type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(operation_description="List equals/groups.",manual_parameters=[configuration])
    def get(self, request):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                configurationId = request.GET['configuration']
                queryset = Equal.objects.filter(groupDestination__configuration=configurationId)
                if queryset.first() is None:
                    queryset = Equal.objects.filter(groupOrigin__configuration=configurationId)
            except:
                queryset = Equal.objects.all()
            serializer = EqualSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Create an equal/group where you analyze that two groups are equal.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = EqualSerializer(data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
