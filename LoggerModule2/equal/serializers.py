from rest_framework import serializers
from Server.models import Equal,Group
from project.serializers import ShortProjectSerializer
from utils.Assertions import *
from utils.strings import Strings
from django.db import transaction



class EqualSerializer(serializers.ModelSerializer):

    class Meta:
        model = Equal
        fields = ('id','groupOrigin','groupDestination')

    def validate(self, attrs):
        data = attrs

        Assertions.assert_true_raise(data.get('groupOrigin')!=data.get('groupDestination'), 401, ('ERROR_SAME_GROUP'))
        Assertions.assert_true_raise(data.get('groupOrigin')!=data.get('groupDestination'), 401, ('ERROR_SAME_GROUP'))

        return data

    '''
    Save a equal group and validate it
    '''
    @transaction.atomic
    def save(self, **kwargs):
        data = self.data
        try:
            pk = data["id"]
        except:
            pk = None
        origin = Group.objects.filter(pk=data["groupOrigin"]).first()
        destination = Group.objects.filter(pk=data["groupDestination"]).first()
        Assertions.assert_true_raise(origin.configuration == destination.configuration, 400,('ERROR_GROUP_CONFIGURATION'))
        if pk is None:
            equal = Equal(groupDestination=destination, groupOrigin=origin, groupDestination_id=destination.id,
                          groupOrigin_id=origin.id)
            equal = equal.save()
        else:
            Assertions.assert_true_raise(False, 400,('ERROR_UPDATE_GROUP'))
        return equal