from rest_framework import serializers
from Server.models import Event
from utils.Assertions import *
from utils.strings import Strings
import time
import re

class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ('id','nameAPP','type','coordinate','duration','content','clipboard','size','timestamp','capture')

    def validate(self, attrs):

            data = attrs

            if data.get("nameAPP"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('nameAPP'),255),401, ('ERROR_NAMEAPP'))
            if data.get("type"):
                Assertions.assert_true_raise(Strings.check_type(data.get('type')),401, ('ERROR_TYPE'))
            if data.get("coordinate"):
                valid = False
                x = (data.get("coordinate"))[0]
                y = (data.get("coordinate"))[1]
                size_X = (data.get("size"))[0]
                size_Y = (data.get("size"))[1]
                if (x <= size_X) and (y <= size_Y):
                    valid = True
                Assertions.assert_true_raise(valid,401,('ERROR_COORDINATE'))
            if data.get("duration"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('duration'), 86400000),401, ('ERROR_DURATION'))
            if data.get("content"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('content'), 10000000),401,('ERROR_CONTENT'))
            if data.get("clipboard"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('clipboard'), 1000000),401,('ERROR_CLIPBOARD'))
            if data.get("timestamp"):
                Assertions.assert_true_raise((int(round((time.time() * 1000))>=data.get("timestamp"))),401,('ERROR_TIMESTAMP'))
            return data

