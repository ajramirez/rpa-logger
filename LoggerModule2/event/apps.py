from django.apps import AppConfig

class Event(AppConfig):
    name = 'events'