from django.db import models
import time
from django.core.validators import MinValueValidator, MaxValueValidator, FileExtensionValidator
from decimal import Decimal
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField

def rename(filename, instance):
    import secrets
    ext = instance.split('.')[-1]
    urlSafe = secrets.token_urlsafe(32)
    screenshot = Screenshot.objects.filter(imagePath='screenshots/capture/'+urlSafe+"."+ext).first()
    if screenshot is None:
        name = '{}.{}'.format('screenshots/capture/'+urlSafe,ext)
        return name
    else:
        rename(filename,instance)

def renameSimilarity(filename, instance):
    import secrets
    ext = instance.split('.')[-1]
    urlSafe = secrets.token_urlsafe(32)
    screenshot = Similarity.objects.filter(picturePath='configuration/similarities/'+urlSafe+"."+ext).first()
    if screenshot is None:
        name = '{}.{}'.format('configuration/similarities/'+urlSafe,ext)
        return name
    else:
        renameSimilarity(filename,instance)

type = (
    ('KEYBOARD','KEYBOARD'),
    ('MOUSE','MOUSE'),
    ('CLIPBOARD','CLIPBOARD'),
    ('SCREENSHOT', 'SCREENSHOT')
)


def get_time_stamp():
    return int(round(time.time() * 1000))

class Log(models.Model):
    creationMoment = models.DateTimeField(auto_now_add=True)
    lastModification = models.DateTimeField(auto_now=True)
    isHidden = models.BooleanField(default=False)

    class Meta:
        abstract = True

class SendPolicy(Log):
    name = models.CharField(max_length=255, blank=False, null=False)
    eventNumber = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100000)])
    connectionTime = models.IntegerField( blank=False,null=False,validators=[MinValueValidator(0), MaxValueValidator(100000)])
    imageLimit = models.BooleanField(default=False)
    description = models.TextField()
    default = models.BooleanField(default=False,blank=False,null=False)

    class Meta:
        verbose_name = 'Send Policy'
        verbose_name_plural = 'Send Policies'

    def __str__(self):
        return str(self.name)

class CapturePolicy(Log):
    name = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField()
    default = models.BooleanField(default=False, blank=False, null=False)

    class Meta:
        verbose_name = 'Capture Policy'
        verbose_name_plural = 'Capture Policies'

    def __str__(self):
        return str(self.name)

class Module(Log):
    name = models.CharField(choices=type, max_length=50)
    capturePolicy = models.ForeignKey(CapturePolicy,related_name="modules", on_delete=models.CASCADE)


    def __str__(self):
        return str(self.name)

class Keyboard(Log):
    keywordNumber = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100000000000)],null=True)

class Mouse(Log):
    pass

class Image(Log):
    changeApp = models.BooleanField(default=False, blank=False, null=False)
    periodTime = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100000000000)],null=True)
    inactiveTime = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100000)],null=True)

class Action(Log):
    keyNumber = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100000)],null=True,blank=True)
    triggerKey = models.CharField(max_length=255,blank=True,null=True)
    keyboard = models.OneToOneField(Keyboard,blank=True,null=True, on_delete=models.CASCADE)
    mouse = models.OneToOneField(Mouse,blank=True,null=True, on_delete=models.CASCADE)
    image = models.OneToOneField(Image,blank=True,null=True, on_delete=models.CASCADE)
    capturePolicy = models.ForeignKey(CapturePolicy,related_name="actions", on_delete=models.CASCADE)

class Computer(Log):
    MAC = models.CharField(max_length=17,unique=True)
    RAM = models.DecimalField(max_digits=12, decimal_places=2, default=0.0,validators=[MinValueValidator(Decimal('0.0'))])
    primaryDisk = models.DecimalField(max_digits=12, decimal_places=2, default=0.0,validators=[MinValueValidator(Decimal('0.0'))])
    machine = models.CharField(max_length=255)
    processor = models.CharField(max_length=255)
    core = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100000)])
    system = models.CharField(max_length=255)
    release = models.CharField(max_length=255)
    version = models.CharField(max_length=255)
    namePC = models.CharField(max_length=255)
    userPC = models.CharField(max_length=255)
    active = models.BooleanField(default=False,blank=False,null=False)

    def __str__(self):
        return str(self.MAC)

class LoggerAuthentication(Log):
    token = models.CharField(max_length=255,unique=True)
    computer = models.OneToOneField(Computer, blank=False, null=False,on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.token)

class Actor(Log):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #computers = models.ManyToManyField(Computer)

    def __str__(self):
        return str(self.user.username)

class Capture(Log):
    timestamp =  models.BigIntegerField(default=get_time_stamp(), validators=[MinValueValidator(0)])
    project = models.ForeignKey("Project",related_name='captures', on_delete=models.DO_NOTHING)
    computer = models.ForeignKey("Computer", on_delete=models.DO_NOTHING)
    status = models.BooleanField(default=True,blank=False,null=False)
    saveEvent = models.BooleanField(default=False,blank=False,null=False)
    saveScreenshot = models.BooleanField(default=False,blank=False,null=False)

    def __str__(self):
        return str(self.timestamp)

class Event(Log):
    nameAPP = models.CharField(max_length=255, blank=True)
    type = models.CharField(choices=type,max_length=50)
    coordinate = ArrayField(models.IntegerField(), blank=True,null=True)
    duration = models.DecimalField(max_digits=16, blank=True, null=True, decimal_places=4, default=0.0, validators=[MinValueValidator(Decimal('0.0'))])
    content = models.CharField(max_length=255)
    clipboard = models.TextField(blank=True)
    size = ArrayField(models.IntegerField(), blank=True,null=True)
    timestamp =  models.BigIntegerField(default=get_time_stamp(), validators=[MinValueValidator(0)])
    capture = models.ForeignKey(Capture,related_name="events", on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.type)

class Screenshot(Log):
    imagePath = models.ImageField(unique=True,upload_to=rename)
    timestamp =  models.BigIntegerField(default=get_time_stamp(), validators=[MinValueValidator(0)])
    capture = models.ForeignKey(Capture,related_name="screenshots", on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.timestamp)

class Project(Log):
    projectName = models.CharField(max_length=255)
    description = models.TextField(blank=False, null=False)
    computers = models.ManyToManyField(Computer,related_name="project")
    sendPolicy = models.ForeignKey(SendPolicy, on_delete=models.DO_NOTHING)
    capturePolicy = models.ForeignKey(CapturePolicy, on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.projectName)

class Configuration(Log):
    umbral = models.DecimalField(max_digits=12, decimal_places=2, default=0.0,validators=[MinValueValidator(Decimal('0.0'))])
    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.umbral)

class Group(Log):
    screenshots = models.ManyToManyField(Screenshot,blank=True)
    events = models.ManyToManyField(Event,blank=True)
    configuration = models.ForeignKey(Configuration, on_delete=models.CASCADE)

class Process(Log):
    groups = models.ManyToManyField(Group)

    class Meta:
        verbose_name = 'Process'
        verbose_name_plural = 'Processes'

class Delete(Log):
    group = models.OneToOneField(Group,related_name="deleteGroup",null=False, blank=False, on_delete=models.CASCADE)

class Equal(Log):
    groupOrigin = models.OneToOneField(Group, related_name="groupOrigin", null=False, blank=False,on_delete=models.CASCADE)
    groupDestination = models.OneToOneField(Group, related_name="groupDestination",null=False, blank=False, on_delete=models.CASCADE)

class Note(Log):
    note = models.TextField(blank=False, null=False)
    configuration = models.ForeignKey(Configuration,related_name="notes", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.note)

class Similarity(Log):
    picturePath = models.ImageField(unique=True,upload_to=renameSimilarity)
    configuration = models.ForeignKey(Configuration,related_name="similarities", on_delete=models.CASCADE)
    screenshots = models.ManyToManyField(Screenshot,blank=True)

    class Meta:
        verbose_name = 'Similarity'
        verbose_name_plural = 'Similarities'

    def __str__(self):
        return str(self.picturePath)

class OCR(Log):
    textOCR = models.CharField(max_length=255,blank=False, null=False)
    configuration = models.ForeignKey(Configuration,related_name="OCRS", on_delete=models.CASCADE)
    screenshots = models.ManyToManyField(Screenshot,blank=True)

    def __str__(self):
        return str(self.textOCR)

class Server_configuration(Log):
    serverAPI = models.CharField(max_length=255,blank=True, null=True)
    serverProcessing = models.CharField(max_length=255,blank=True, null=True)
    serverRPA = models.CharField(max_length=255,blank=True, null=True)
    serverFrontend = models.CharField(max_length=255,blank=True, null=True)
    serverAPIToken = models.CharField(max_length=255,blank=True, null=True)
    serverProcessingToken = models.CharField(max_length=255,blank=True, null=True)
    serverRPAToken = models.CharField(max_length=255,blank=True, null=True)
