from rest_framework import serializers
from Server.models import Similarity, Server_configuration
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *
from requests_toolbelt.multipart.encoder import MultipartEncoder
import requests


class SimilaritySerializer(serializers.ModelSerializer):

    class Meta:
        model = Similarity
        fields = ('id','picturePath','configuration')

    def validate(self, attrs):

            data = attrs

            if data.get("picturePath"):
                img_size = len(data.get("picturePath"))
                Assertions.assert_true_raise(img_size <= 2097152, 401, 'ERROR_SIMILARITY_LENGHT')
            return data

'''
Check the template matching one by one with the screenshots and save the result from the processing server
'''
def processing_data_similarity(similarity):
    try:
        project = similarity.configuration.project
        captures = Capture.objects.filter(project=project)
        screenshots = []
        if captures is not None:
            for capture in captures:
                screenshotsDB = Screenshot.objects.filter(capture=capture)
                for i in screenshotsDB:
                    screenshots.append(i)
            if similarity is not None and screenshots:
                for screenshot in screenshots:
                    res = call_similarity(similarity, screenshot)
                    if res and res is not None:
                        similarity.screenshots.add(screenshot.id)
        return similarity
    except:
        return None

'''
Calls the template matching function of the post-processing server
'''
def call_similarity(similar,imagen):
    try:
        server = str((Server_configuration.objects.all().first()).serverProcessing)+"/similarity/"
        file = {
            'image': (imagen.imagePath.name, open(imagen.imagePath.path, 'rb'), 'text/plain'),
            'template': (similar.picturePath.name, open(similar.picturePath.path, 'rb'), 'text/plain')
            }
        multipart_data = MultipartEncoder(file)
        header = {'Content-Type': multipart_data.content_type,'Server-token':(Server_configuration.objects.all().first()).serverAPIToken}
        result = requests.post(server, data=multipart_data, headers=header)
        if result.status_code == 200:
            return result
        else:
            return None
    except:
        return None