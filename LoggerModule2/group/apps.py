from django.apps import AppConfig

class Group(AppConfig):
    name = 'groups'