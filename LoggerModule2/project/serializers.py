from rest_framework import serializers
from Server.models import Project, Capture, SendPolicy, CapturePolicy
from utils.Assertions import *
from utils.strings import Strings
from computer.serializers import *
from capture.serializers import *
from capturePolicy.serializers import *
from sendPolicy.serializers import *


class ProjectConfigurationSerializer(serializers.ModelSerializer):
    captures = CaptureConfigurationSerializer(many=True,read_only=True)

    class Meta:
        depth = 1
        model = Project
        fields = ('id','projectName','captures')

class ShortProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ('id','projectName','description')

class ConnectionProjectSerializer(serializers.ModelSerializer):
    capturePolicy = CapturePolicySerializer()
    sendPolicy = SendPolicySerializer()
    captures = CaptureStatusSerializer()

    class Meta:
        depth = 1
        model = Project
        fields = ('id','projectName','description','capturePolicy','sendPolicy','captures')

class ProjectSerializer(serializers.ModelSerializer):
    captures = CaptureSerializer(many=True, read_only=True)

    class Meta:
        model = Project
        fields = ('id','projectName','description','capturePolicy','sendPolicy','captures','computers')

    def validate(self, attrs):

            data = attrs

            if data.get("projectName"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('projectName'), 254),401, ('ERROR_PROJECT_NAME_LENGHT'))
            if data.get("description"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('description'), 1000000000),401, ('ERROR_DESCRIPTION_LENGHT'))
            return data
