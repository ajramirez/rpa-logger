from rest_framework import serializers
from Server.models import Image, Capture, SendPolicy, CapturePolicy
from utils.Assertions import *
from utils.strings import Strings

class ImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ('id','changeApp','periodTime','inactiveTime')

    def validate(self, attrs):

            data = attrs

            if data.get("changeApp"):
                Assertions.assert_true_raise((data.get('changeApp') is False or True),401, ('ERROR_CHANGEAPP_TYPE'))
            if data.get("periodTime"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('periodTime'), 100000000000),401, ('ERROR_PERIODTIME_LENGHT'))
            if data.get("inactiveTime"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('inactiveTime'), 100000000000), 401,('ERROR_INACTIVETIME_LENGHT'))
            return data

