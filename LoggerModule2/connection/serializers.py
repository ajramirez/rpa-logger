from rest_framework import serializers
from Server.models import Project, Computer, LoggerAuthentication, Configuration, Group, Delete, Equal, Similarity, OCR
from utils.Assertions import *
from utils.strings import Strings
from project.serializers import *
from capturePolicy.serializers import *
from sendPolicy.serializers import *
from project.serializers import *
import secrets
import datetime
import csv
from django.http import HttpResponse
from rest_framework import status
from django.db import transaction


'''
Generates a token for the PC connection
'''
@transaction.atomic
def token_save(request):
    tokenAuto = secrets.token_hex(64)

    computer = Computer.objects.filter(MAC=(request.data['MAC']).upper()).first()
    if computer is not None:
        logToken = LoggerAuthentication(token=tokenAuto, computer=computer)
        logToken.save()
        return logToken


'''
Check the token caducity
'''
def caducity_token(token):
    try:
        logToken = LoggerAuthentication.objects.filter(token=token).first()
        today24 = datetime.datetime.now() - datetime.timedelta(days = 1)
        creation = logToken.creationMoment
        import pytz
        utc = pytz.UTC
        today24 = utc.localize(today24)
        if logToken is not None:
            if creation < today24:
                logToken.delete()
    except:
        return None
