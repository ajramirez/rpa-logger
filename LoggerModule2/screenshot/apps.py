from django.apps import AppConfig

class Screenshot(AppConfig):
    name = 'screenshots'