from rest_framework import status
from Server.models import CapturePolicy
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema


class CapturePolicyManager(generics.RetrieveAPIView):
    queryset = CapturePolicy.objects.all()
    serializer_class = CapturePolicySerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return CapturePolicy.objects.get(pk=pk)
        except CapturePolicy.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get the capture policy by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            capturePolicy = self.get_object(pk)
            serializer = CapturePolicySerializer(capturePolicy)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


    @swagger_auto_schema(operation_description="Delete the capture policy by id and change the projects to the default capture policy.")
    @transaction.atomic
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            capturePolicy = self.get_object(pk)
            if capturePolicy.default is True:
                return Response(status=status.HTTP_409_CONFLICT)
            else:
                if capturePolicy is not None:
                    project = Project.objects.filter(capturePolicy=capturePolicy)
                    capturePolicy_default = CapturePolicy.objects.filter(default=True).first()
                    if project:
                        for i in project:
                            i.capturePolicy = capturePolicy_default
                            i.save()
                    capturePolicy.delete()
                    return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class ListCapturePolicy(generics.ListCreateAPIView):
    serializer_class = CapturePolicySerializer

    def get_queryset(self):
        return CapturePolicy.objects.all()

    @swagger_auto_schema(operation_description="List the capture policies.")
    def get(self, request):

        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            queryset = CapturePolicy.objects.all()
            serializer = CapturePolicySerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Create a capture policy. There can only be one default. If a default policy exist, modify it.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                serializer = CapturePolicySerializer(data=request.data,partial=False)
                default_validator(None, request.data)
                if serializer.validate(request.data):
                    serializer.is_valid()
                    capturePolicy = serializer.save()
                    serialized  = CapturePolicySerializer(capturePolicy)
                    return Response(serialized.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
