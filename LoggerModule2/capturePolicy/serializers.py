from rest_framework import serializers
from Server.models import Project, Capture, SendPolicy, CapturePolicy
from utils.Assertions import *
from utils.strings import Strings
from module.serializers import *
from action.serializers import *
from django.db import transaction


class CapturePolicySerializer(serializers.ModelSerializer):
    modules = ShortModuleSerializer(many=True)
    actions = ShortActionSerializer(many=True)

    class Meta:
        depht = 1
        model = CapturePolicy
        fields = ('id','name','description','default','modules','actions')

    '''
    The capture policy is sent in a single json
    Save the modules one by one
    Save actions of any type (mouse, keyboard, image) one by one
    '''
    @transaction.atomic
    def save(self, **kwargs):
        data = self.data
        name = data["name"]
        description = data["description"]
        default = data["default"]
        try:
            pk = self._kwargs['data']['id']
        except:
            pk= None
        if pk is None:
            capturePolicy = CapturePolicy(name=name, description=description, default=default)
        else:
            capturePolDB = CapturePolicy.objects.filter(pk=pk).first()
            capturePolicy = CapturePolicy(pk=pk,name=name, description=description, default=default,creationMoment=capturePolDB.creationMoment)
        capturePolicy.save()
        modules = data["modules"]
        if modules is not None:
            for module in modules:
                try:
                    pk_mo = module['id']
                    module["capturePolicy"] = capturePolicy
                except:
                    pk_mo = None
                    module["capturePolicy"] = capturePolicy.id
                if pk_mo is None:
                    serializer = ModuleSerializer(data=module, partial=True)
                    serializer.capturePolicy=capturePolicy
                    if serializer.validate(module):
                        serializer.is_valid()
                        serializer.save()
                else:
                    moduleBD = Module.objects.filter(pk=pk_mo).first()
                    serializer = ModuleSerializer(moduleBD, data=module, partial=True)
                    if serializer.validate(module):
                        serializer.is_valid()
                        moduleBD.name=module['name']
        actions = data["actions"]
        if actions is not None:
            for action in actions:
                try:
                    pk_act = action['id']
                    action["capturePolicy"] = capturePolicy.id
                except:
                    pk_act = None
                    action["capturePolicy"] = capturePolicy
                if pk_act is None:
                    serializer = ActionSerializer(data=action, partial=True)
                    if serializer.validate(action):
                        serializer.is_valid()
                        serializer.save()
                else:
                    actionBD = Action.objects.filter(pk=pk_act).first()
                    serializer = ActionSerializer(actionBD, data=action, partial=True)
                    if serializer.validate(action):
                        serializer.is_valid()
                        serializer.save()

        return capturePolicy

    def validate(self, attrs):

            data = attrs
            if data.get("name"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('name'), 119),401, ('ERROR_NAME_LENGHT'))
            if data.get("description"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('description'), 1000),401, ('ERROR_DESCRIPTION_LENGHT'))
            return data

'''
Check if there is a predefined policy or if the policy being modified is the prefined policy 
'''
def default_validator(pk,data):
    if data.get("default"):
        default = CapturePolicy.objects.filter(default=True).first()
        if default is not None and pk is not None:
            Assertions.assert_true_raise(int(pk) == default.id, 400, ('ERROR_DEFAULT_EXIST'))
        else:
            Assertions.assert_true_raise(False, 400, ('ERROR_DEFAULT_EXIST'))

