from django.apps import AppConfig

class CapturePolicy(AppConfig):
    name = 'capturePolicies'