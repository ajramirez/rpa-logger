from rest_framework import serializers
from Server.models import OCR, Server_configuration
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *
from requests_toolbelt.multipart.encoder import MultipartEncoder
import requests

class OCRSerializer(serializers.ModelSerializer):

    class Meta:
        model = OCR
        fields = ('id','textOCR','configuration')

    def validate(self, attrs):

            data = attrs

            if data.get("textOCR"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('textOCR'),1000000000),401, 'ERROR_OCR_TEXT')
            return data

'''
Check the ocr one by one with the screenshots and save the result from the processing server
'''
def processing_data_ocr(ocr):
    try:
        project = ocr.configuration.project
        captures = Capture.objects.filter(project=project)
        screenshots = []
        if captures is not None:
            for capture in captures:
                screenshotsDB = Screenshot.objects.filter(capture=capture)
                for i in screenshotsDB:
                    screenshots.append(i)
            if ocr is not None and screenshots:
                for screenshot in screenshots:
                    res = call_ocr(ocr.textOCR, screenshot)
                    if res and res is not None:
                        ocr.screenshots.add(screenshot.id)
        return ocr
    except:
        return None

'''
Calls the OCR function of the post-processing server
'''
def call_ocr(text,imagen):
    try:
        server = str((Server_configuration.objects.all().first()).serverProcessing)+"/ocr/"
        file = {
            'text': text,
            'image': (imagen.imagePath.name, open(imagen.imagePath.path, 'rb'), 'text/plain')}
        multipart_data = MultipartEncoder(file)
        header = {'Content-Type': multipart_data.content_type,'Server-token':(Server_configuration.objects.all().first()).serverAPIToken}
        result = requests.post(server, data=multipart_data, headers=header)
        if result.status_code == 200:
            return result
        else:
            return None
    except:
        return None