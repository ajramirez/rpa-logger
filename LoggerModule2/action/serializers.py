from rest_framework import serializers
from Server.models import Action, Mouse
from utils.Assertions import *
from utils.strings import Strings
from image.serializers import *
from keyboard.serializers import *
from django.db import transaction
from django.db import models

class ActionSerializer(serializers.ModelSerializer):
    image = ImageSerializer(many=False)
    keyboard = KeyboardSerializer(many=False)

    class Meta:
        depht = 1
        model = Action
        fields = ('id','keyNumber','triggerKey','keyboard','mouse','image','capturePolicy')

    '''
    Save actions of any type (mouse, keyboard, image)
    The capture policy is sent in a single json
    '''
    @transaction.atomic
    def save(self, **kwargs):
        data = self.data
        if data["keyboard"] is not None:
            type = 'keyboard'
        elif data["mouse"]is not None:
            type = 'mouse'
        elif data["image"] is not None:
            type = 'image'
        keyNumber = data["keyNumber"]
        triggerKey = data["triggerKey"]
        try:
            pk = self._kwargs['data']['id']
            capturePolicy = CapturePolicy.objects.filter(pk=data["capturePolicy"]).first()
        except:
            pk= None
            capturePolicy = data["capturePolicy"]
        if type=="keyboard":
            keyb = data["keyboard"]
            keyb
            try:
                pk_key = keyb["id"]
            except:
                pk_key = None
            if pk_key is None:
                serializer = KeyboardSerializer(data=keyb, partial=True)
                if serializer.validate(keyb):
                    serializer.is_valid()
                    serializer.save()
            else:
                keybBD = Keyboard.objects.filter(pk=pk_key).first()
                serializer = KeyboardSerializer(keybBD, data=keyb, partial=True)
                if serializer.validate(keyb):
                    serializer.is_valid()
                    keybBD.keywordNumber= keyb['keywordNumber']
        if type=="image":
            ima = data["image"]
            try:
                pk_im = ima["id"]
            except:
                pk_im = None
            if pk_im is None:
                serializer = ImageSerializer(data=ima, partial=True)
                if serializer.validate(ima):
                    serializer.is_valid()
                    serializer.save()
            else:
                imagebBD = Image.objects.filter(pk=pk_im).first()
                serializer = ImageSerializer(imagebBD, data=ima, partial=True)
                if serializer.validate(ima):
                    serializer.is_valid()
                    imagebBD.changeApp = ima['changeApp']
                    imagebBD.periodTime = ima['periodTime']
                    imagebBD.inactiveTime = ima['inactiveTime']
        if type=="mouse":
            mous = data["mouse"]
            try:
                pk_m = mous["id"]
            except:
                pk_m = None
            if pk_m is None:
                serializer = Mouse()
                serializer.save()
            else:
                serializer = Mouse(id=pk_m)
                serializer.save()
        if pk is None:
            if type == "keyboard":
                keyBoard= Keyboard.objects.filter(pk=serializer.data['id']).first()
                action = Action(keyNumber=keyNumber, triggerKey=triggerKey,
                                capturePolicy=capturePolicy, keyboard=keyBoard)
            if type == "image":
                imag= Image.objects.filter(pk=serializer.data['id']).first()
                action = Action(keyNumber=keyNumber, triggerKey=triggerKey,
                                capturePolicy=capturePolicy,image=imag)
            if type == "mouse":
                mouse= Mouse.objects.filter(pk=serializer.id).first()
                action = Action(keyNumber=keyNumber, triggerKey=triggerKey,
                                capturePolicy=capturePolicy,mouse=mouse)
        else:
            if type == "keyboard":
                keyBoard= Keyboard.objects.filter(pk=serializer.data['id']).first()
                action = Action(pk=pk,keyNumber=keyNumber, triggerKey=triggerKey,
                                capturePolicy=capturePolicy, keyboard=keyBoard,creationMoment=keyBoard.creationMoment)
            if type == "image":
                imag= Image.objects.filter(pk=serializer.data['id']).first()
                action = Action(pk=pk,keyNumber=keyNumber, triggerKey=triggerKey,
                                capturePolicy=capturePolicy,image=imag,creationMoment=imag.creationMoment)
            if type == "mouse":
                mouse= Mouse.objects.filter(pk=serializer.data['id']).first()
                action = Action(pk=pk,keyNumber=keyNumber, triggerKey=triggerKey,
                                capturePolicy=capturePolicy,mouse=mouse,creationMoment=mouse.creationMoment)
        action.save()


    def validate(self, attrs):

            data = attrs

            if data.get("keyNumber"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('keyNumber'), 1000000000000),401, ('ERROR_KEYNUMBER_LENGHT'))
            return data

class ShortActionSerializer(serializers.ModelSerializer):
    image = ImageSerializer(many=False)
    keyboard = KeyboardSerializer(many=False)

    class Meta:
        depht = 1
        model = Action
        fields = ('id','keyNumber','triggerKey','keyboard','mouse','image')
