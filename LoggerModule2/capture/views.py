from rest_framework import status
from Server.models import Capture, Event, Screenshot, Configuration, Computer
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema


class CaptureManager(generics.RetrieveUpdateAPIView):
    queryset = Capture.objects.all()
    serializer_class = CaptureSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Capture.objects.get(pk=pk)
        except Capture.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get a capture by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            capture = self.get_object(pk)
            if capture is None:
                return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                serializer = CaptureSerializer(capture)
                return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Modify a capture by id. You can stop/start a capture if you change the status. There can only be one active capture per computer.")
    def put(self, request, pk=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            capture = self.get_object(pk)
            captureBD = Capture.objects.filter(computer=capture.computer, status=True).first()
            request.data['timestamp'] = capture.timestamp
            request.data['project'] = capture.project.id
            request.data['computer'] = capture.computer.id
            if (captureBD is None) or (request.data['status']==False) or (capture.status==True and capture.id == captureBD.id):
                serializer = CaptureCreateSerializer(capture, data=request.data, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                Assertions.assert_true_raise(False, 409, "An active capture already exist")
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Delete a capture by id and the screenshots and events asociated.")
    @transaction.atomic
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            capture = self.get_object(pk)
            configuration = Configuration.objects.filter(project=capture.project).first()
            if configuration is None:
                events = Event.objects.filter(capture=pk)
                screenshots = Screenshot.objects.filter(capture=pk)
                events.delete()
                screenshots.delete()
                capture.delete()
                return Response(status=status.HTTP_200_OK)
            else:
                Assertions.assert_true_raise(False, 409, "A configuration use this capture")
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class CreateCapture(generics.CreateAPIView):
    serializer_class = CaptureCreateSerializer

    def get_queryset(self):
        return Capture.objects.all()

    @swagger_auto_schema(operation_description="Create a capture by id. There can only be one active capture per computer.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            request.data['timestamp'] = int(round(time.time() * 1000))
            serializer = CaptureCreateSerializer(data=request.data,partial=True)
            capture = Capture.objects.filter(computer=request.data["computer"],status=True).first()
            if capture is None or request.data['status']==False:
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                Assertions.assert_true_raise(False, 409, "An active capture already exist")
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)