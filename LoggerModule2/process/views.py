from rest_framework import generics
from Server.models import Process
from .serializers import *
from utils.Assertions import *
from utils.authentication_utils import get_logged_user, get_server_token_access
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework import status

class ProcessManager(generics.RetrieveAPIView):
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer

    @swagger_auto_schema(operation_description="Get a process by id.")
    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Process.objects.get(pk=pk)
        except Process.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get a process by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        serverRPA = get_server_token_access(request, "RPA")
        if loggedUser is not None or serverRPA is not None:
            if pk is None:
                pk = self.kwargs['pk']
            process = self.get_object(pk)
            serializer = ProcessSerializer(process)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListProcess(generics.ListCreateAPIView):
    serializer_class = ProcessCreateSerializer

    def get_queryset(self):
        return Process.objects.all()

    @swagger_auto_schema(operation_description="List the process.")
    def get(self, request):
        loggedUser = get_logged_user(request)
        serverRPA = get_server_token_access(request, "RPA")
        if loggedUser is not None or serverRPA is not None:
            queryset = Process.objects.all()
            serializer = ProcessSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(auto_schema=None, operation_description="Create a process. Only allowed for the RPA server.")
    def post(self, request, *args, **kwargs):
        serverRPA = get_server_token_access(request, "RPA")
        if serverRPA is not None:
            serializer = ProcessCreateSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            Assertions.assert_true_raise(False,404,"User not found")