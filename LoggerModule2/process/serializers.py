from rest_framework import serializers
from Server.models import Process
from group.serializers import GroupSerializer

class ProcessSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True)

    class Meta:
        depth = 1
        model = Process
        fields = ('id','groups')

class ProcessCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Process
        fields = ('id','groups')
