from rest_framework.authtoken.models import Token
from Server.models import User, Computer, LoggerAuthentication, Server_configuration

def get_logged_user(request):
    try:
        token = request.headers["authorization"]
        if token is not None:
            token_object = Token.objects.all().filter(pk=token).first()
            if token_object is not None:
                user = token_object.user
                if user.is_active:
                    return user
                else:
                    return None
        else:
            return None
    except:
        return None

def get_token_computer(request):
    try:
        token = request.headers["authorization"]
        if token is not None:
            token_object = LoggerAuthentication.objects.all().filter(token=str(token)).first()
            if token_object is not None:
                computer = token_object.computer
                if computer.active:
                    return computer
                else:
                    return None
        else:
            return None
    except:
        return None

def get_server_token_access(request,server_type):
    try:
        server = None
        if server_type == "PROCESSING":
            server = (Server_configuration.objects.all().first()).serverProcessingToken
        if server_type == "RPA":
            server = (Server_configuration.objects.all().first()).serverRPAToken
        value = request.META['HTTP_SERVER_TOKEN']
        if str(value) == server:
            return True
        else:
            return None
    except:
        return None