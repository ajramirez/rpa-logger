from Server.models import Server_configuration

class Configuration_server:

    @staticmethod
    def server_configuration(text):
        try:
            if text is not None:
               server = Server_configuration.objects.all().first()
               return server[text]
            else:
                return False
        except:
            return False