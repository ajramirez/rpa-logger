# Method from a previous project (https://github.com/grooving) that can help in validators
import re

class Strings:

    @staticmethod
    def check_max_length(cadena, max_length):
        try:
            if cadena is not None:
                if len(cadena) <= max_length:
                    return True
                else:
                    return False
            else:
                return True
        except:
            return False

    @staticmethod
    def check_mac(mac):
        try:
            x = re.search("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$", mac)
            if x:
                return True
            else:
                return False
        except:
            return False

    @staticmethod
    def check_number_max(number, max_length):
        try:
            if number <= max_length:
                return True
            else:
                return False
        except:
            return False

    @staticmethod
    def check_type(type_data):
        type = ('KEYBOARD', 'MOUSE','CLIPBOARD', 'SCREENSHOT')
        try:
            if type_data in type:
                return True
            else:
                return False
        except:
            return False

