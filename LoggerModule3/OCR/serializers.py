from rest_framework import serializers
from utils.Assertions import *
from .OCR import optical_character_recognition

class OCRSerializer(serializers.Serializer):
    text = serializers.CharField(required=True, max_length=255)
    image = serializers.ImageField()

    class Meta:
        fields = ('text','image')

'''
Separates the data obtained from the image and text to call the OCR process
'''
def ocr_process(request):
    try:
        text = request.data['text']
        image = request.FILES["image"].read()
        if optical_character_recognition(text,image):
            return True
        else:
            return False
    except:
        return None
