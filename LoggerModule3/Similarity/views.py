from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from utils.Assertions import Assertions
from .serializers import *
from utils.utils import *
from LoggerModule3.settings import SERVER_PROCESSING_TOKEN

'''
Returns whether an image contains a template image with a true or false
'''
class SimilarityManager(generics.CreateAPIView):
    serializer_class = SimilaritySerializer

    def post(self, request, pk=None, format=None):
        if get_server_validation(request):
            valor = similarity_process(request)
            header = {'Server-token': SERVER_PROCESSING_TOKEN}
            if valor is not None:
                return Response(valor, status=status.HTTP_200_OK,headers=header)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST,headers=header)
        else:
            return Assertions.assert_true_raise(False, 404, "Server not valid")
