from rest_framework import serializers
from utils.Assertions import *
from .template import template_mathcing
from requests_toolbelt.multipart import decoder

class SimilaritySerializer(serializers.Serializer):
    template = serializers.ImageField()
    image = serializers.ImageField()

    class Meta:
        fields = ('template','image')

'''
Separates the data obtained from the image and the template image to call the OCR process
'''
def similarity_process(request):
    try:
        template = request.FILES["template"].read()
        image = request.FILES["image"].read()
        return template_mathcing(image,template)
    except:
        return None

