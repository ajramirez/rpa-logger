from django.apps import AppConfig

class Similarity(AppConfig):
    name = 'similarities'