from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from utils.Assertions import Assertions
from .serializers import *
from utils.utils import *
from LoggerModule3.settings import SERVER_PROCESSING_TOKEN

'''
Group screenshots by similarity threshold
'''
class FingerPrintManager(generics.CreateAPIView):
    serializer_class = ConfigurationGroupSerializer

    def post(self, request, pk=None, format=None):
        if get_server_validation(request):
            valor = fingerPrint_process(request)
            if valor is not None:
                header = {'Content-Type': 'text/plain','Server-token': SERVER_PROCESSING_TOKEN}
                return Response(valor,status=status.HTTP_200_OK,headers=header)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Assertions.assert_true_raise(False,404,"Server not valid")
