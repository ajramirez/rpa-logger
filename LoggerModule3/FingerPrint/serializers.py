from rest_framework import serializers
from utils.Assertions import *
from .fingerPrint import finger_print_group
from LoggerModule3.settings import SERVER_API, SERVER_PROCESSING_TOKEN
import requests
import json
import time

def get_time_stamp():
    return int(round(time.time() * 1000))

class ScreenshotSerializer(serializers.Serializer):
    imagePath = serializers.ImageField()
    id = serializers.IntegerField(required=True)
    capture = serializers.IntegerField(required=True)
    timestamp =  serializers.IntegerField(default=get_time_stamp())

    class Meta:
        fields = ('id', 'imagePath', 'timestamp', 'capture')


class CaptureConfigurationSerializer(serializers.Serializer):
    screenshots = ScreenshotSerializer(many=True,read_only=True)
    id = serializers.IntegerField(required=True)
    timestamp =  serializers.IntegerField(default=get_time_stamp())

    class Meta:
        depth = 1
        fields = ('id','timestamp','screenshots')


class ProjectConfigurationSerializer(serializers.Serializer):
    captures = CaptureConfigurationSerializer(many=True,read_only=True)
    id = serializers.IntegerField(required=True)
    projectName = serializers.CharField(required=True,  max_length=255)

    class Meta:
        depth = 1
        fields = ('id','projectName','captures')


class ConfigurationGroupSerializer(serializers.Serializer):
    project = ProjectConfigurationSerializer(many=False,read_only=True)
    umbral = serializers.FloatField(required=True)
    id = serializers.IntegerField(required=True)

    class Meta:
        depht = 1
        fields = ('id','umbral','project')

'''
Function that separates the information received and uses it to get the screenshots and make 
the grouping by similarity with the configuration obtained
'''
def fingerPrint_process(request):
    try:
        data = json.loads(request.data)
        images = {}
        configurationId = data['id']
        umbral = data['umbral']
        captures = data['project']['captures']
        for cap in captures:
            screenshots = cap['screenshots']
            for scr in screenshots:
                (id,image) = get_image_screenshot(scr['id'])
                if image is not None and id is not None:
                    images[id] = image
        imageList = finger_print_group(umbral, images)
        if imageList:
            postList = []
            for i in imageList:
                i[1]
                dict = {}
                dict["configuration"] = configurationId
                screenshots = []
                for i in i[1]:
                    screen = {}
                    screen["id"] = i
                    screenshots.append(screen)
                dict["screenshots"] = screenshots
                postList.append(dict)
            post_group(postList)
            return imageList
        else:
            return None
    except:
        return None
'''
Create groups on the server after similarity grouping
'''
def post_group(dict):
    try:
        jsonFile = json.dumps(dict)
        server = SERVER_API+"/groups/"
        header = {'Content-Type': 'application/json','Server-token':SERVER_PROCESSING_TOKEN}
        result = requests.post(server, data=jsonFile, headers=header)
        if result.status_code == 200:
            return result
        else:
            return None
    except:
        return None

'''
You have the route of the images. With this function you get the images from the server one by one 
'''
def get_image_screenshot(id):
    try:
        server = SERVER_API+"/screenshots/"+str(id)+"/"
        header = {'Server-token':SERVER_PROCESSING_TOKEN}
        result = requests.get(server, headers=header)
        if result.status_code == 200:
            result.raw.decode_content = True
            image = result.content
            return id,image
        else:
            return None, None
    except:
        return None, None

