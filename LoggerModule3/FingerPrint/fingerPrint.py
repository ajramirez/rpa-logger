import pytesseract as ts
import cv2 as cv
import numpy as np
import os
from PIL import Image
import imagehash
import distance
import io

'''
It groups images by a similarity threshold. The threshold is the percentage 
of similarity being 0.0 allowing everything and 1.0 being equal
We pre-treat the image and then compare by hamming distance
'''
def finger_print_group(umbral, images):
    imagesHashList = []
    umbral = float(umbral)
    for i in images:
        id = i
        image = images[i]
        img = Image.open(io.BytesIO(image))
        hashimg = str(imagehash.dhash(img))
        value = True
        for imgList in imagesHashList:
            for img in imgList[0]:
                distHamm = distance.hamming(img, hashimg)
                if float(distHamm) <= len(hashimg)*umbral:
                    imgList[0].append(hashimg)
                    imgList[1].append(id)
                    value = False
                    break
        if value:
            idList = [id]
            hashList = [hashimg]
            imagesHashList.append([hashList,idList])
    return imagesHashList
