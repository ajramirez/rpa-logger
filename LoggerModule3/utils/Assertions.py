from rest_framework import serializers
from rest_framework.exceptions import APIException

class Assertions:

    @staticmethod
    def assert_true_raise(assertion: bool, error_code, details: dict = {"message": "No authenticate"}):
        Assertions.__assert_true(assertion, error_code, "message", details)

    @staticmethod
    def __assert_true(assertion: bool, http_error: int, code: dict, details: dict):
        if not assertion:
            print(code)
            print(details)
            exception = APIException(code=code, detail=details)
            exception.status_code = http_error
            raise exception
